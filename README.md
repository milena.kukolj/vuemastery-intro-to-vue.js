# VueMastery Intro to Vue.js

I followed [this](https://www.vuemastery.com/courses/intro-to-vue-js/) course 
and build a simple web-app that displays products. I used Vue 
components and learned what it means for a framework to be reactive.